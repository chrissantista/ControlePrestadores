package sistema.interceptor;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import sistema.model.Usuario;

public class AutorizadorInterceptor extends HandlerInterceptorAdapter{
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String uri = request.getRequestURI();
		if (eUriLiberada(uri))
			return true;
		
		if (uri.startsWith(request.getContextPath() + "/resources"))
			return true;
		
		HttpSession session =  request.getSession();
		Usuario usuario = (Usuario) session.getAttribute("usuarioLogado");
		if (usuario != null)
			return true;
		response.sendRedirect(request.getContextPath() + "/login");
		return false;
	}
	
	private boolean eUriLiberada(String uriRequest) {
		List<String> urisLiberadas = new ArrayList<String>();
		
		urisLiberadas.add("login");
		urisLiberadas.add("efetuaLogin");
		urisLiberadas.add("resources");
		
		for (String uri : urisLiberadas) {
			if (uriRequest.endsWith(uri))
				return true;
		}
		return false;
	}
}
