package sistema.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Prestador {
	
	@Id
	@GeneratedValue
	private long id;
	
	@NotNull
	@NotEmpty
	@Size(min=5)
	private String nome;
	
	@CPF
	@Column(unique=true)
	private String cpf;
	
	@ManyToOne
	@NotNull
	private Empresa empresa;
	
	@NotNull
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Calendar dataIntegracao;
	
	@NotNull
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Calendar dataExame;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Calendar getDataIntegracao() {
		return dataIntegracao;
	}

	public void setDataIntegracao(Calendar dataIntegracao) {
		this.dataIntegracao = dataIntegracao;
	}
	
	public void setDataIntegracao(String dataIntegracao) {
		this.dataIntegracao = stringToCalendar(dataIntegracao);
	}

	public Calendar getDataExame() {
		return dataExame;
	}

	public void setDataExame(Calendar dataExame) {
		this.dataExame = dataExame;
	}
	
	public void setDataExame(String dataExame) {
		this.dataExame = stringToCalendar(dataExame);
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	private Calendar stringToCalendar(String dataString) {
		try {
			Date data = new SimpleDateFormat("dd/MM/yyyy").parse(dataString);
			Calendar dataCalendar = Calendar.getInstance();
			dataCalendar.setTimeInMillis(data.getTime());
			return dataCalendar;
		} catch (ParseException e) {
			throw new RuntimeException();
		}
	}
}
