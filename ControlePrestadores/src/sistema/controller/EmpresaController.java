package sistema.controller;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;

import sistema.dao.EmpresaDao;
import sistema.model.Empresa;

@Transactional
@Controller
public class EmpresaController {

	@Autowired
	@Qualifier("jpaEmpresaDao")
	private EmpresaDao dao;
	
	@RequestMapping("listaEmpresas")
	public String lista(Model model) {
		model.addAttribute("empresas", dao.lista());
		return "empresa/lista";
	}
	
	@RequestMapping("pesquisaEmpresas")
	public String pesquisa(Model model, String razao) {
		model.addAttribute("empresas", dao.listaPorRazao(razao));
		return "empresa/lista";
	}
	
	@RequestMapping("novaEmpresa")
	public String form() {
		return "empresa/formulario";
	}
	
	@RequestMapping("adicionaEmpresa")
	public String adiciona(@Valid Empresa empresa, BindingResult result) {
		if (cnpjJaExiste(empresa))
			result.addError(new FieldError("empresa", "cnpj", "CNPJ j� cadastrado"));
		if (result.hasErrors())
			return "empresa/formulario";
		dao.cadastra(empresa);
		return "redirect:listaEmpresas";
	}
	
	@RequestMapping("removeEmpresa")
	public void remove(Empresa empresa, HttpServletResponse response) {
		dao.remove(empresa.getId());
		response.setStatus(200);
	}
	
	@RequestMapping("mostraEmpresa")
	public String mostra(long id, Model model) {
		model.addAttribute("empresa", dao.buscaPorId(id));
		return "empresa/mostra";
	}
	
	@RequestMapping("mostraEmpresaPorCnpj")
	public String mostraPorCnpj(String cnpj, Model model) {
		model.addAttribute("empresa", dao.buscaPorCnpj(cnpj));
		return "empresa/mostra";
	}
	
	@RequestMapping("alteraEmpresa")
	public String altera(@Valid Empresa empresa, BindingResult result) {
		if (cnpjJaExiste(empresa))
			result.addError(new FieldError("empresa", "cnpj", "CNPJ j� cadastrado"));
		if (result.hasErrors()) {
			return "empresa/mostra";
		}
		dao.altera(empresa);
		return "redirect:listaEmpresas";
	}
	
	private boolean cnpjJaExiste(Empresa empresa) {
		Empresa empresaDb = dao.buscaPorCnpj(empresa.getCnpj());
		if (empresaDb == null)
			return false;
		return empresaDb.getId() != empresa.getId();
	}
}
