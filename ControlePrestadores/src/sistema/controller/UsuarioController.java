package sistema.controller;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;

import sistema.dao.UsuarioDao;
import sistema.model.Usuario;

@Transactional
@Controller
public class UsuarioController {

	@Autowired
	@Qualifier("jpaUsuarioDao")
	UsuarioDao dao;
	
	@RequestMapping("login")
	public String form() {
		Usuario admin = dao.busca("admin");
		if (admin == null) {
			admin = new Usuario();
			admin.setLogin("admin");
			admin.setSenha("123456");
			admin.setConfirmaSenha("123456");
			admin.setNome("Administrador");
			dao.adiciona(admin);
		}
		return "login-form";
	}
	
	@RequestMapping("menu")
	public String menu() {
		return "menu-principal";
	}
	
	@RequestMapping("efetuaLogin")
	public String login(Usuario usuario, HttpSession session) {
		if (!autenticaUsuario(usuario))
			return "login-form";
		session.setAttribute("usuarioLogado", dao.busca(usuario.getLogin()));
		return "redirect:menu";
	}
	
	@RequestMapping("logoff")
	public String logoff(HttpSession session) {
		session.invalidate();
		return "redirect:login";
	}
	
	@RequestMapping("listaUsuarios")
	public String lista(Model model) {
		model.addAttribute("usuarios", dao.lista());
		return "usuario/lista";
	}
	
	@RequestMapping("novoUsuario")
	public String novo() {
		return "usuario/formulario";
	}
	
	@RequestMapping("adicionaUsuario")
	public String adiciona(@Valid Usuario usuario, BindingResult result) {
		boolean confereSenha = usuario.getSenha().equals(usuario.getConfirmaSenha());
		if (!confereSenha)
			result.addError(new FieldError("usuario", "senha", "Confirma��o de senha n�o confere."));
		boolean loginJaExiste = dao.busca(usuario.getLogin()) != null;
		if (loginJaExiste)
			result.addError(new FieldError("usuario", "login", "Login j� existe."));
		if (result.hasErrors())
			return "usuario/formulario";
		
		dao.adiciona(usuario);
		return "redirect:listaUsuarios";
	}
	
	@RequestMapping("mostraUsuario")
	public String mostra(String login, Model model) {
		model.addAttribute("usuario", dao.busca(login));
		return "usuario/mostra";
	}
	
	@RequestMapping("alteraUsuario")
	public String altera(@Valid Usuario usuario, BindingResult result) {
		boolean confereSenha = usuario.getSenha().equals(usuario.getConfirmaSenha());
		if (!confereSenha)
			result.addError(new FieldError("usuario", "senha", "Confirma��o de senha n�o confere."));
		if (result.hasErrors())
			return "usuario/mostra";
		dao.altera(usuario);
		return "redirect:listaUsuarios";
	}
	
	@RequestMapping("removeUsuario")
	public void remove(Long id, HttpServletResponse response) {
		dao.remove(id);
		response.setStatus(200);
	}
	
	private boolean autenticaUsuario(Usuario usuario) {
		Usuario usuarioDb = dao.busca(usuario.getLogin());
		if (usuarioDb == null)
			return false;
		return usuarioDb.getSenha().equals(usuario.getSenha());
	}
}
