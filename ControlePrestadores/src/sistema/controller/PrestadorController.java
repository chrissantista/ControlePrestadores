package sistema.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;

import sistema.dao.EmpresaDao;
import sistema.dao.PrestadorDao;
import sistema.model.Prestador;

@Controller
@Transactional
public class PrestadorController {
	
	@Autowired
	@Qualifier("jpaPrestadorDao")
	private PrestadorDao dao;
	
	@Autowired
	@Qualifier("jpaEmpresaDao")
	private EmpresaDao empresaDao;
	
	@RequestMapping("listaPrestadores")
	public String lista(Model model) {
		model.addAttribute("prestadores", dao.lista());
		return "prestador/lista";
	}
	
	@RequestMapping("prestadoresPorEmpresa")
	public String pesquisaPorEmpresa(Model model, long idEmpresa) {
		model.addAttribute("prestadores", dao.listaPorEmpresa(idEmpresa));
		return "prestador/lista";
	}
	
	@RequestMapping("prestadoresPorNome")
	public String pesquisaPorNome(Model model, String nome) {
		model.addAttribute("prestadores", dao.listaPorNome(nome));
		return "prestador/lista";
	}
	
	@RequestMapping("novoPrestador")
	public String form(Model model) {
		model.addAttribute("empresas", empresaDao.lista());
		return "prestador/formulario";
	}
	
	@RequestMapping("adicionaPrestador")
	public String adiciona(@Valid Prestador prestador, BindingResult result, Model model) {
		prestador.setEmpresa(empresaDao.buscaPorId(prestador.getEmpresa().getId()));
		model.addAttribute("empresas", empresaDao.lista());
		if (cpfJaExiste(prestador))
			result.addError(new FieldError("prestador", "cpf", "CPF j� cadastrado."));
		if (result.hasErrors())
			return "prestador/formulario";
		dao.adiciona(prestador);
		return "redirect:listaPrestadores";
	}
	
	@RequestMapping("mostraPrestador")
	public String mostra(long id, Model model) {
		model.addAttribute("empresas", empresaDao.lista());
		model.addAttribute("prestador", dao.buscaPorId(id));
		return "prestador/mostra";
	}
	
	@RequestMapping("mostraPrestadorCpf")
	public String mostra(String cpf, Model model) {
		model.addAttribute("empresas", empresaDao.lista());
		model.addAttribute("prestador", dao.buscaPorCpf(cpf));
		return "prestador/mostra";
	}
	
	@RequestMapping("alteraPrestador")
	public String altera(@Valid Prestador prestador, BindingResult result, Model model) {
		prestador.setEmpresa(empresaDao.buscaPorId(prestador.getEmpresa().getId()));
		model.addAttribute("empresas", empresaDao.lista());
		if(cpfJaExiste(prestador))
			result.addError(new FieldError("prestador", "cpf", "CPF j� cadastrado."));
		if (result.hasErrors())
			return "prestador/mostra";
		dao.altera(prestador);
		return "redirect:listaPrestadores";
	}
	
	@RequestMapping("removePrestador")
	public void remove(long id, HttpServletResponse response) {
		dao.remove(id);
		response.setStatus(200);
	}
	
	private boolean cpfJaExiste(Prestador prestador) {
		Prestador prestadorDb = dao.buscaPorCpf(prestador.getCpf());
		if (prestadorDb == null)
			return false;
		return prestadorDb.getId() != prestador.getId();
	}
	
}
