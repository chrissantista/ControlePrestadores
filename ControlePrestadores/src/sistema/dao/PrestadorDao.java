package sistema.dao;

import java.util.List;

import sistema.model.Prestador;

public interface PrestadorDao {
	void adiciona(Prestador prestador);
	void altera(Prestador prestador);
	void remove(long id);
	Prestador buscaPorId(long id);
	Prestador buscaPorCpf(String cpf);
	List<Prestador> lista();
	List<Prestador> listaPorEmpresa(long idEmpresa);
	List<Prestador> listaPorNome(String nome);
}
