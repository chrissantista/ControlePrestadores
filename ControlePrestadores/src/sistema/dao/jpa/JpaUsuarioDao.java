package sistema.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import sistema.dao.UsuarioDao;
import sistema.model.Usuario;

@Repository
public class JpaUsuarioDao implements UsuarioDao {
	
	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public void adiciona(Usuario usuario) {
		manager.persist(usuario);
	}

	@Override
	public void altera(Usuario usuario) {
		manager.merge(usuario);
	}

	@Override
	public void remove(Long id) {
		Usuario usuario = manager.find(Usuario.class, id);
		manager.remove(usuario);
	}
	
	@Override
	public Usuario busca(Long id) {
		return manager.find(Usuario.class, id);
	}
	
	@Override
	public Usuario busca(String login) {
		TypedQuery<Usuario> query = manager.createQuery(
				"select u from Usuario u where login = :login", Usuario.class);
		query.setParameter("login", login);
		try{
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Usuario> lista() {
		TypedQuery<Usuario> usuarios = manager.createQuery(
				"select u from Usuario u", Usuario.class);
		return usuarios.getResultList();
	}

}
