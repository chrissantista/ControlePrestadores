package sistema.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import sistema.dao.EmpresaDao;
import sistema.model.Empresa;

@Repository
public class JpaEmpresaDao implements EmpresaDao {
	
	@PersistenceContext
	EntityManager manager;
	
	@Override
	public void cadastra(Empresa empresa) {
		manager.persist(empresa);
	}

	@Override
	public void altera(Empresa empresa) {
		manager.merge(empresa);
	}

	@Override
	public void remove(long id) {
		Empresa empresa = manager.find(Empresa.class, id);
		manager.remove(empresa);
	}

	@Override
	public Empresa buscaPorId(long id) {
		return manager.find(Empresa.class, id);
	}
	
	@Override
	public Empresa buscaPorCnpj(String cnpj) {
		TypedQuery<Empresa> query = manager.createQuery(
				"select e from Empresa e where cnpj = :cnpj", Empresa.class);
		query.setParameter("cnpj", cnpj);
		try {
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
		
	}
	
	@Override
	public List<Empresa> lista() {
		TypedQuery<Empresa> query = manager.createQuery(
				"select e from Empresa e", Empresa.class);
		return query.getResultList();
	}

	@Override
	public List<Empresa> listaPorRazao(String razao) {
		TypedQuery<Empresa> query = manager.createQuery(
				"select e from Empresa e where razao like :razao", Empresa.class);
		query.setParameter("razao", razao + "%");
		return query.getResultList();
	}

}
