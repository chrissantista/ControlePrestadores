package sistema.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import sistema.dao.PrestadorDao;
import sistema.model.Prestador;

@Repository
public class JpaPrestadorDao implements PrestadorDao {
	
	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public void adiciona(Prestador prestador) {
		manager.persist(prestador);
	}

	@Override
	public void altera(Prestador prestador) {
		manager.merge(prestador);
	}

	@Override
	public void remove(long id) {
		Prestador prestador = manager.find(
				Prestador.class, id);
		manager.remove(prestador);
	}

	@Override
	public Prestador buscaPorId(long id) {
		return manager.find(Prestador.class, id);
	}

	@Override
	public List<Prestador> lista() {
		return manager.createQuery(
				"select p from Prestador p", Prestador.class).getResultList();
	}

	@Override
	public List<Prestador> listaPorEmpresa(long idEmpresa) {
		TypedQuery<Prestador> query =  
				manager.createQuery("select p from Prestador p "
				+ "where empresa_id = :idEmpresa", Prestador.class);
		query.setParameter("idEmpresa", idEmpresa);
		return query.getResultList();
		
	}

	@Override
	public List<Prestador> listaPorNome(String nome) {
		TypedQuery<Prestador> query = manager.createQuery(
				"select p from Prestador p "
				+ "where nome like :nome", Prestador.class);
		query.setParameter("nome", nome + "%");
		return query.getResultList();
	}

	@Override
	public Prestador buscaPorCpf(String cpf) {
		TypedQuery<Prestador> query = manager.createQuery(
				"select p from Prestador p where cpf = :cpf", 
				Prestador.class);
		query.setParameter("cpf", cpf);
		try {
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
		
	}

}
