package sistema.dao;

import java.util.List;

import sistema.model.Usuario;

public interface UsuarioDao {
	void adiciona(Usuario usuario);
	void altera(Usuario usuario);
	void remove(Long id);
	Usuario busca(Long id);
	Usuario busca(String login);
	List<Usuario> lista();
}
