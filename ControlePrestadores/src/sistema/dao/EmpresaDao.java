package sistema.dao;

import java.util.List;

import sistema.model.Empresa;

public interface EmpresaDao {
	void cadastra(Empresa empresa);
	void altera(Empresa empresa);
	void remove(long id);
	Empresa buscaPorId(long id);
	Empresa buscaPorCnpj(String cnpj);
	List<Empresa> lista();
	List<Empresa> listaPorRazao(String razao);
}
