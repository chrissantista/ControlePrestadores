<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="resources/css/login.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sistema de Controle de Prestadores</title>
</head>
<body>

<form class="form" action="efetuaLogin" method="post">

<div class="topform" >Sistema de Controle de Prestadores</div>
<div class="bottomform">
<input class="txtfield" type="text" name="login" value="${usuario.login}" placeholder="Login"/>
<input class="txtfield" type="password" name="senha" placeholder="Senha"/>
<input class="botao" type="submit" value="Efetuar login" />
</div>
</form>

</body>
</html>