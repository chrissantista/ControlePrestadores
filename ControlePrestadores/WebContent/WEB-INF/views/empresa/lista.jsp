<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="resources/css/style.css" rel="stylesheet">
<script src="resources/js/jquery-3.1.1.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lista de Empresas</title>
</head>
<body>

<script type="text/javascript">
	function removeEmpresa(id) {
		var r = confirm("Deseja realmente excluir?");
		if (r == true) {
			$.post("removeEmpresa", {'id' : id}, function() {
				$("#empresa_"+id).closest("tr").hide();
			})
		}
	}
</script>

<h3>Lista de Empresas</h3>

<a class="btn btn-green" href="novaEmpresa">Adicionar</a>

<table class="tabela">
	<tr>
		<th>ID</th>
		<th>Raz�o Social</th>
		<th>CNPJ</th>
		<th></th>
		<th></th>
	</tr>
	<c:forEach items="${empresas}" var="empresa">
		<tr id="empresa_${empresa.id}">
			<td>${empresa.id}</td>
			<td>${empresa.razao}</td>
			<td>${empresa.cnpj}</td>
			<td><a class="btn btn-orange" href="mostraEmpresa?id=${empresa.id}">alterar</a></td>
			<td><a class="btn btn-red" href="#" onclick="removeEmpresa(${empresa.id})">remover</a></td>
		</tr>
	</c:forEach>
</table>

<a class="btn" href="menu" >Menu</a>

</body>
</html>