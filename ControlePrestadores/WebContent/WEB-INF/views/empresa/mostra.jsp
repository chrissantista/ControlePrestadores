<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="resources/css/style.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Alterar empresa</title>
</head>
<body>

<h3>Alterar empresa</h3>

<form action="alteraEmpresa" method="post" >
	<input type="hidden" name="id" value="${empresa.id}" />

	<label>Raz�o Social</label>
	<input class="form-control" type="text" name="razao" value="${empresa.razao}" /><br>
	<form:errors path="empresa.razao" class="erro"/>

	<label>CNPJ</label>
	<input class="form-control" type="text" name="cnpj" value="${empresa.cnpj}" /><br>
	<form:errors path="empresa.cnpj" class="erro"/>
	
	<input class="btn btn-orange" type="submit" value="Alterar" />
</form>

<p><a class="btn" href="listaEmpresas" >Voltar</a></p>

</body>
</html>