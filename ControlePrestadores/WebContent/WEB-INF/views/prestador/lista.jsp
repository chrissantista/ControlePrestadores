<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="resources/css/style.css" rel="stylesheet">
<script src="resources/js/jquery-3.1.1.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lista de prestadores</title>
</head>
<body>

<script type="text/javascript">
	function removePrestador(id) {
		var r = confirm("Deseja realmente excluir?");
		if (r == true) {
			$.post("removePrestador", {'id' : id}, function() {
				$("#prestador_"+id).closest("tr").hide();
			})
		}
	}
</script>

<h3>Lista de prestadores</h3>

<a class="btn btn-green" href="novoPrestador">Adicionar</a>

<table class="tabela">
	<tr>
		<th>Id</th>
		<th>Nome</th>
		<th>CPF</th>
		<th>Empresa</th>
		<th>Integração</th>
		<th>Exame</th>
		<th></th>
		<th></th>
	</tr>
	<c:forEach var="prestador" items="${prestadores}">
		<tr id="prestador_${prestador.id}">
			<td>${prestador.id}</td>
			<td>${prestador.nome}</td>
			<td>${prestador.cpf}</td>
			<td>${prestador.empresa.razao}</td>
			<fmt:formatDate value="${prestador.dataIntegracao.time}" pattern="dd/MM/yyyy" var="dataIntegracaoFormatada"/>
			<td>${dataIntegracaoFormatada}</td>
			<fmt:formatDate value="${prestador.dataExame.time}" pattern="dd/MM/yyyy" var="dataExameFormatada"/>
			<td>${dataExameFormatada}</td>
			<td><a class="btn btn-orange" href="mostraPrestador?id=${prestador.id}">alterar</a></td>
			<td><a class="btn btn-red" href="#" onclick="removePrestador(${prestador.id})">remover</a></td>
		</tr>
	</c:forEach>
</table>

<a class="btn" href="menu" >Menu</a>

</body>
</html>