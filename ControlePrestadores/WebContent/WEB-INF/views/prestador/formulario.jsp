<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tg"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="resources/js/jquery-3.1.1.js"></script>
<script src="resources/js/jquery-ui.js"></script>
<script src="resources/js/formatter.js"></script>
<link href="resources/css/jquery-ui.css" rel="stylesheet">
<link href="resources/css/style.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Adicionar prestador</title>
</head>
<body>

<h3>Adicionar prestador</h3>

<form action="adicionaPrestador" method="post">

	<label>Nome</label>
	<input class="form-control" type="text" name="nome" value="${prestador.nome}"><br>
	<form:errors path="prestador.nome" class="erro"/>

	<label>CPF</label>
	<input class="form-control" type="text" name="cpf" value="${prestador.cpf}"><br>
	<form:errors path="prestador.cpf" class="erro"/><br>


	<label>Empresa</label>
	<select class="form-control" name="empresa.id">
		<c:forEach items="${empresas}" var="empresa">
			<c:if test="${empresa.id eq prestador.empresa.id}">
				<option value="${empresa.id}" selected>${empresa}</option>
			</c:if>
			<c:if test="${empresa.id ne prestador.empresa.id}">
				<option value="${empresa.id}">${empresa}</option>
			</c:if>
		</c:forEach>
	</select>
	<form:errors path="prestador.empresa" class="erro"/><br>

	<fmt:formatDate value="${prestador.dataIntegracao.time}" pattern="dd/MM/yyyy" var="dataIntegracaoFormatada" />
	<label>Data limite da integração</label>
	<tg:campoData id="dataIntegracao" value="${dataIntegracaoFormatada}" />
	<form:errors path="prestador.dataIntegracao" class="erro"/><br>

	<fmt:formatDate value="${prestador.dataExame.time}" pattern="dd/MM/yyyy" var="dataExameFormatada" />
	<label>Data limite do exame</label>
	<tg:campoData id="dataExame" value="${dataExameFormatada}" />
	<form:errors path="prestador.dataExame" class="erro"/><br>
	
	<input class="btn btn-gree" type="submit" value="Cadastrar" />
</form>

<p><a class="btn" href="listaPrestadores" >Voltar</a><p>

</body>
</html>