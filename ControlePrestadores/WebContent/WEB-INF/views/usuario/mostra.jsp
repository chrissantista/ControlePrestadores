<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="resources/css/style.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Alterar Usu�rio</title>
</head>
<body>

<h3>Alterar Usu�rio</h3>

<form action="alteraUsuario" method="post">
	
	<input type="hidden" name="id" value="${usuario.id}"/>
	
	<label>Login</label>
	<input type="text" name="login" value="${usuario.login}"/><br>
	<form:errors path="usuario.login" class="erro"/>
	
	<label>Senha</label>
	<input type="password" name="senha" value="${usuario.senha}"/><br>
	<form:errors path="usuario.senha" class="erro"/>
	
	<label>Confirmar senha</label>
	<input type="password" name="confirmaSenha" value="${usuario.confirmaSenha}"/><br>
	<form:errors path="usuario.confirmaSenha" class="erro"/>
	
	<label>Nome</label>
	<input type="text" name="nome" value="${usuario.nome}"/><br>
	<form:errors path="usuario.nome" class="erro"/>
	
	<input class="btn btn-orange" type="submit" value="Alterar"/><br>
	
</form>

<p><a class="btn" href="listaUsuarios" >Voltar</a></p>

</body>
</html>