<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="resources/js/jquery-3.1.1.js"></script>
<link href="resources/css/style.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lista de Usu�rios</title>
</head>
<body>

<script type="text/javascript">
	function removeUsuario(id) {
		var r = confirm("Deseja realmente excluir?");
		if (r == true) {
			$.post("removeUsuario", {'id' : id}, function() {
				$("#usr_"+id).closest("tr").hide();
			});
		}
	}

</script>

<h3>Lista de Usu�rios</h3>

<p><a class="btn btn-green" href="novoUsuario">Adicionar</a></p>
<table class="tabela">
	<tr>
		<th>Id</th>
		<th>Login</th>
		<th>Nome</th>
		<th></th>
		<th></th>
	</tr>
	<c:forEach var="usuario" items="${usuarios}">
		<tr id="usr_${usuario.id}">
			<td>${usuario.id}</td>
			<td>${usuario.login}</td>
			<td>${usuario.nome}</td>
			<td><a class="btn btn-orange" href="mostraUsuario?login=${usuario.login}">alterar</a></td>
			<td><a class="btn btn-red" href="#" onclick="removeUsuario(${usuario.id})">remover</a></td>
		</tr>
	</c:forEach>
</table>

<a class="btn" href="menu" >Menu</a>

</body>
</html>