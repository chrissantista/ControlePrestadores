<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="resources/css/style.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sistema de Controle de Prestadores</title>
</head>
<body>

<h2>Sistema de Controle de Prestadores</h2>

<p>Bem vindo, ${usuarioLogado.nome}!</p>

<a class="btn" href="listaPrestadores" >Prestadores</a>
<a class="btn" href="listaEmpresas" >Empresas</a>
<a class="btn" href="listaUsuarios">Usu�rios</a>

<p><a class="btn btn-red" href="logoff">Logout</a></p>

</body>
</html>