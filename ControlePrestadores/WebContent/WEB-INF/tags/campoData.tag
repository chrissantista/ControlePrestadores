<%@ attribute name="id" required="true" %>
<%@ attribute name="value" required="false" %>
<%@ attribute name="classe" required="false" %>

<input type="text" id="${id}" name="${id}" value="${value}"  class="classe" maxlength="10"/><br>
<script>
	$("#${id}").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd/mm/yy'
	});
</script>
